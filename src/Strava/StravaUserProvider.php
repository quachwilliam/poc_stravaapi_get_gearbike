<?php

namespace App\Strava;

use App\Security\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StravaUserProvider 
{
    private $stravaClient;
    private $stravaId;
    private $httpClient;

    /**
     * User in active session
     * @var User
     */
    private $userSecurity;
    /**
     * StravaUserProvider Constructor
     *
     * @param $stravaClient
     * @param $stravaId
     * @param $httpClient
     */
    public function __construct($stravaClient,$stravaId, HttpClientInterface $httpClient, Security $security)
    {
        $this->stravaClient = $stravaClient;
        $this->stravaId = $stravaId;
        $this->httpClient = $httpClient;
        $this->userSecurity = $security->getUser();

    }

    public function loadUserFromStrava(string $code) {
        $url = sprintf("https://www.strava.com/oauth/token?client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code",
        $this->stravaId, $this->stravaClient, $code);

        $response = $this->httpClient->request('POST', $url, [
            'headers' => [
                'Accept' => "application/json"
            ]
        ]);

        $token = $response->toArray()['access_token'];

        $responseBike = $this->httpClient->request('GET', 'https://www.strava.com/api/v3/gear/b6705102', [
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ] );
        $responseUser = $this->httpClient->request('GET', 'https://www.strava.com/api/v3/athlete', [
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ] );

        $dataBike = ($responseBike->toArray());
        $dataUser = ($responseUser->toArray());

        return new User($dataBike, $dataUser);
    }
}