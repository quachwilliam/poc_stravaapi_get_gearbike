<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoginController extends AbstractController
{
    private $stravaId;

    public function __construct($stravaId)
    {
        $this->stravaId = $stravaId;
    } 
    /**
     * @Route("/login", name="login")
     */
    public function index()
    {
        return $this->render('login/index.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }

    /**
     * @Route("/login/strava", name="strava")
     */
    public function strava(UrlGeneratorInterface $generator)
    {
        $url = $generator->generate('dashboard', [], UrlGeneratorInterface::ABSOLUTE_URL);
        return new RedirectResponse("https://www.strava.com/oauth/authorize?client_id=$this->stravaId&response_type=code&redirect_uri=".$url);
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(HttpClientInterface $httpClient)
    {
        return $this->render('login/dashboard.html.twig');
    }
}
