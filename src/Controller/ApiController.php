<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(HttpClientInterface $httpClient)
    {
        $response = $httpClient->request('GET', 'https://api.github.com/users/williamquach/repos');
        // dd($response->toArray());
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
            'repos' => $response->toArray(),
        ]);
    }

    /**
     * @Route("/show", name="show")
     */
    public function show(HttpClientInterface $httpClient)
    {
        $response = $httpClient->request('GET', 'https://api.github.com/users/williamquach/repos');
        // dd($response->toArray());
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
            'repos' => $response->toArray(),
        ]);
    }
}
