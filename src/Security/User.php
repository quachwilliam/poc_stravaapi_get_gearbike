<?php

namespace App\Security;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private $username;

    private $roles = [];

    private $idBike;

    private $nomBike;

    private $descriptionBike;

    private $distanceBike;    

    private $nom;  

    private $prenom;    

    public function __construct(array $dataBike = [], array $dataUser = [])
    {
        $this->idBike = $dataBike['id'];
        $this->nomBike = $dataBike['name'];
        $this->descriptionBike = $dataBike['description'];
        $this->distanceBike = ($dataBike['distance'] / 1000);
        $this->username = $dataUser['username'];
        $this->nom = $dataUser['lastname'];
        $this->prenom = $dataUser['firstname'];
        
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Get the value of idBike
     */ 
    public function getIdBike()
    {
        return $this->idBike;
    }

    /**
     * Get the value of nomBike
     */ 
    public function getNomBike()
    {
        return $this->nomBike;
    }

    /**
     * Get the value of descriptionBike
     */ 
    public function getDescriptionBike()
    {
        return $this->descriptionBike;
    }

    /**
     * Get the value of distanceBike
     */ 
    public function getDistanceBike()
    {
        return $this->distanceBike;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the value of prenom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }
}
